# DiscordWTF
Discord Bot for user interaction in the CodeMonkeyWTF discord server.

## About the bot...
This goal of this bot is to host a text and image based tower defense game that members can play against each other.

## Progress
- [ ] Ping/Pong
- [ ] Sqlite set up
- [ ] Drop system
- [ ] Battle system
- [ ] +/- XP system
- [ ] Images for embeds
